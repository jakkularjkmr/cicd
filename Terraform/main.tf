provider "aws" {
  region = var.aws_region
}
# creating ec2 instance
resource "aws_instance" "web" {
  ami           = "ami-0759f51a90924c166"
  instance_type = "t2.micro"
  tags = {
    Name = "web-instance"
  }
}

# #create s3 bucket
# resource "aws_s3_bucket" "mybucket" {
#   bucket = "statefilebucket333"
# }

# resource "aws_s3_bucket_versioning" "versioning_mybucket" {
#   bucket = aws_s3_bucket.mybucket.id
#   versioning_configuration {
#     status = "Enabled"
#   }
# }

# resource "aws_s3_bucket_server_side_encryption_configuration" "s3bucket" {
#   bucket = aws_s3_bucket.mybucket.id

#   rule {
#     apply_server_side_encryption_by_default {
#       sse_algorithm     = "AES256"
#     }
#   }
# }

# #Create dynamodb

# resource "aws_dynamodb_table" "dynamodb-table" {
#   name           = "State-file-lock"
#   billing_mode   = "PAY_PER_REQUEST"
#   hash_key       = "LockId"
 

#   attribute {
#     name = "LockId"
#     type = "S"
#   }
# }  
